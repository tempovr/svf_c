cmake_minimum_required(VERSION 3.0.0)
project(svf_c VERSION 0.1.0)
include(ExternalProject)

include(CTest)
enable_testing()
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
cmake_policy(SET CMP0057 NEW)
cmake_policy(SET CMP0082 NEW)
cmake_policy(SET CMP0065 NEW)
set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

set(CMAKE_CXX_FLAGS_DEBUG
    "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g -fno-omit-frame-pointer"# -fsanitize=address" # add
                                                                                 # leakcheck
                                                                                 # here and
                                                                                 # weap
)

set(CMAKE_C_FLAGS_DEBUG
    "${CMAKE_C_FLAGS_DEBUG} -O0 -g -fno-omit-frame-pointer"# -fsanitize=address" # add
                                                                                 # leakcheck
                                                                                 # here and
                                                                                 # weap
)

set(CMAKE_LINKER_FLAGS_DEBUG
    "${CMAKE_LINKER_FLAGS_DEBUG} -O0 -g -fno-omit-frame-pointer"# -fsanitize=address "
)

add_subdirectory(external/SVF)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/external/SVF/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/external/boost_1_72_0)
include_directories(/usr/lib/ocaml)
add_subdirectory(lib)

add_subdirectory(tests)

include(CPack)
