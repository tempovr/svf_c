; ModuleID = 'simple_loop.c'
source_filename = "simple_loop.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: noinline nounwind sspstrong uwtable
define dso_local i64 @loop_until(i64 %bound) #0 {
entry:
  %retval = alloca i64, align 8
  %bound.addr = alloca i64, align 8
  %b = alloca i64, align 8
  %a = alloca i64, align 8
  store i64 %bound, i64* %bound.addr, align 8
  %0 = load i64, i64* %bound.addr, align 8
  %cmp = icmp eq i64 %0, 3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i64 7, i64* %b, align 8
  %1 = load i64, i64* %b, align 8
  store i64 %1, i64* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  store i64 1, i64* %a, align 8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %2 = load i64, i64* %a, align 8
  %3 = load i64, i64* %bound.addr, align 8
  %cmp1 = icmp slt i64 %2, %3
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i64, i64* %a, align 8
  %inc = add nsw i64 %4, 1
  store i64 %inc, i64* %a, align 8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i64 1, i64* %retval, align 8
  br label %return

return:                                           ; preds = %while.end, %if.then
  %5 = load i64, i64* %retval, align 8
  ret i64 %5
}

; Function Attrs: noinline nounwind sspstrong uwtable
define dso_local void @expensive_function() #0 {
entry:
  %a = alloca i64, align 8
  %call = call i64 @loop_until(i64 100)
  store i64 %call, i64* %a, align 8
  ret void
}

; Function Attrs: noinline nounwind sspstrong uwtable
define dso_local void @cheap_function() #0 {
entry:
  %num_times = alloca i32, align 4
  %b = alloca i64, align 8
  store i32 10, i32* %num_times, align 4
  %0 = load i32, i32* %num_times, align 4
  %conv = sext i32 %0 to i64
  %call = call i64 @loop_until(i64 %conv)
  store i64 %call, i64* %b, align 8
  ret void
}

; Function Attrs: noinline nounwind sspstrong uwtable
define dso_local void @func(i32 %a) #0 {
entry:
  %a.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @expensive_function()
  br label %if.end

if.else:                                          ; preds = %entry
  call void @cheap_function()
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

attributes #0 = { noinline nounwind sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{!"clang version 9.0.1 "}
