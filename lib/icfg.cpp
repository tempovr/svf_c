#include "icfg.h"
#include "pag.h"
#include "wrappers.h"

#include "SVF-FE/LLVMUtil.h"
#include "WPA/Andersen.h"
#include "llvm-c/Core.h"
#include "llvm-c/Support.h"
#include "caml/alloc.h"
#include "caml/custom.h"
#include "caml/memory.h"
#include "caml/fail.h"
#include "caml/callback.h"

icfg_t get_icfg(pag_t grph) {
  /*  ICFG *iter_over = grph->grph->getICFG();
    for (auto ptr : *iter_over) {
      ICFGNode *nd = ptr.second;
      if (FunExitBlockNode *exit_nd = llvm::dyn_cast<FunExitBlockNode>(nd)) {
        if (exit_nd->getFormalRet() == 0) {
          auto fun = exit_nd->getFun();
          std::cout << fun->getName().str() << std::endl;
          assert(false);
        }
      }
    }*/
  return new icfg{grph->grph->getICFG()};
}

icfg_iter_t get_icfg_iter(icfg_t cfg) {
  return new icfg_iter{cfg->grph->end(), cfg->grph->begin()};
}

void icfg_next(icfg_iter_t iter) { iter->iter++; }

icfg_node_t icfg_curr(icfg_iter_t iter) {
  assert(iter->iter != iter->end);
  auto nop = iter->iter->second->getFun()->getLLVMFun();
  return new icfg_node{iter->iter->second};
}

void icfg_intra_addrprint(icfg_node_t nd) {
  std::cout << nd->nd << std::endl;
}

unsigned icfg_node_id(icfg_node_t nd) { return nd->nd->getId(); }

ICFGNodeType get_icfg_node_ty(icfg_node_t nd) {
  std::cout << nd->nd << std::endl;
  switch (nd->nd->getNodeKind()) {
  case ICFGNode::ICFGNodeK::IntraBlock:
    return ICFGNodeType::IntraBlock;
  case ICFGNode::ICFGNodeK::FunCallBlock:
    return ICFGNodeType::FunCallBlock;
  case ICFGNode::ICFGNodeK::FunEntryBlock:
    return ICFGNodeType::FunEntryBlock;
  case ICFGNode::ICFGNodeK::FunExitBlock:
    return ICFGNodeType::FunExitBlock;
  case ICFGNode::ICFGNodeK::FunRetBlock:
    return ICFGNodeType::FunRetBlock;
  default:
    assert(false);
  }
}

bool icfg_iter_end(icfg_iter_t iter) { return iter->iter == iter->end; }

void icfg_iter_destroy(icfg_iter_t iter) { delete iter; }

CAMLprim LLVMValueRef icfg_node_get_function(value nd_int) {
  icfg_node_t nd = (icfg_node_t) Nativeint_val(nd_int);
  return llvm::wrap(nd->nd->getFun()->getLLVMFun());
}




CAMLprim LLVMValueRef icfg_intra_insn(value nd_int) {
  icfg_node_t nd = (icfg_node_t) Nativeint_val(nd_int);
  std::cout << nd->nd << std::endl;
  LLVMValueRef val =  llvm::wrap(llvm::cast<IntraBlockNode>(nd->nd)->getInst());
  std::cout << std::string(LLVMPrintValueToString(val))<<  std::endl;
  return val;
}

icfg_pag_edge_iter_t icfg_intra_edges(icfg_node_t nd) {
  return new icfg_pag_edge_iter{
      llvm::cast<IntraBlockNode>(nd->nd)->getPAGEdges().end(),
      llvm::cast<IntraBlockNode>(nd->nd)->getPAGEdges().begin()};
}

icfg_pag_nd_iter_t icfg_funentry_params(icfg_node_t nd) {
  return new icfg_pag_nd_iter{
      llvm::cast<FunEntryBlockNode>(nd->nd)->getFormalParms().end(),
      llvm::cast<FunEntryBlockNode>(nd->nd)->getFormalParms().begin()};
}

CAMLprim LLVMValueRef icfg_call_getcallsite(value nd_int) {
  icfg_node_t nd = (icfg_node_t) Nativeint_val(nd_int);
  return llvm::wrap(
      llvm::cast<CallBlockNode>(nd->nd)->getCallSite().getInstruction());
}

icfg_pag_nd_iter_t icfg_callblock_params(icfg_node_t nd) {
  assert(llvm::isa<CallBlockNode>(nd->nd));

  return new icfg_pag_nd_iter{
      llvm::cast<CallBlockNode>(nd->nd)->getActualParms().end(),
      llvm::cast<CallBlockNode>(nd->nd)->getActualParms().begin()};
}

pag_node_t icfg_funexit_ret(icfg_node_t nd) {
  const PAGNode *res = llvm::cast<FunExitBlockNode>(nd->nd)->getFormalRet();
  if (res == NULL) {
    return nullptr;
  }

  pag_node_t new_node = new pag_node;
  new_node->nd = res;
  std::cout << "returning out" << std::endl;
  return new_node;
}

pag_node_t icfg_ret_actual(icfg_node_t nd) {
  if (const PAGNode *res = llvm::cast<RetBlockNode>(nd->nd)->getActualRet()) {
    std::cout << "ID: res" << std::endl;
    return new pag_node{res};
  } else {
    std::cout << "is null ret" << std::endl;
    return nullptr;
  }
}

CAMLprim LLVMValueRef icfg_ret_getcallsite(value nd_int) {
  icfg_node_t nd = (icfg_node_t) Nativeint_val(nd_int);
  return llvm::wrap(
      llvm::cast<RetBlockNode>(nd->nd)->getCallSite().getInstruction());
}

bool icfg_pag_nd_iter_end(icfg_pag_nd_iter_t end) {
  return end->iter == end->end;
}

pag_node_t icfg_pag_nd_iter_curr(icfg_pag_nd_iter_t iter) {
  return new pag_node{*iter->iter};
}

void icfg_pag_nd_iter_next(icfg_pag_nd_iter_t iter) { iter->iter++; }

pag_edge_t icfg_pag_edge_iter_curr(icfg_pag_edge_iter_t iter) {
  return new pag_edge{*iter->iter};
}

void icfg_pag_edge_iter_next(icfg_pag_edge_iter_t iter) { iter->iter++; }

bool icfg_pag_edge_iter_end(icfg_pag_edge_iter_t iter) {
  return iter->end == iter->iter;
}

void icfg_pag_nd_iter_destroy(icfg_pag_nd_iter_t iter) { delete iter; }

void icfg_pag_edge_iter_destroy(icfg_pag_edge_iter_t iter) { delete iter; }


icfg_edge_iter_t get_out_edge_iter(icfg_node_t nd) {
  return new icfg_edge_iter{nd->nd->OutEdgeEnd(),nd->nd->OutEdgeBegin()};
}
bool icfg_edge_iter_end(icfg_edge_iter_t iter) {
  return iter->iter == iter->end;
}
icfg_edge_t icfg_edge_curr(icfg_edge_iter_t iter) {
return new icfg_edge{*iter->iter};
}
void icfg_edge_next(icfg_edge_iter_t iter) {
  std::advance(iter->iter,1);
}
void icfg_edge_iter_destroy(icfg_edge_iter_t iter) {
 // delete iter;
}

ICFGEdgeTy icfg_edge_getkind(icfg_edge_t edg) {
  switch(edg->edge->getEdgeKind()) {
    case ICFGEdge::ICFGEdgeK::IntraCF:
      return ICFGEdgeTy::IntraCF;
    case ICFGEdge::ICFGEdgeK::CallCF:
      return ICFGEdgeTy::CallCF;
    case ICFGEdge::ICFGEdgeK::RetCF:
      return ICFGEdgeTy::RetCF;  
    default:
      assert(false);
  }
}

icfg_node_t icfg_edge_src(icfg_edge_t edg) {
  return new icfg_node{edg->edge->getSrcNode()};
}
icfg_node_t icfg_edge_dst(icfg_edge_t edg) {
  return new icfg_node{edg->edge->getDstNode()};
}

CAMLprim LLVMValueRef icfg_ret_edge_callsite(value edg_int) {
  icfg_edge_t edg = (icfg_edge_t) Nativeint_val(edg_int);
  return llvm::wrap(llvm::cast<RetCFGEdge>(edg->edge)->getCallSite().getInstruction());
}

CAMLprim LLVMValueRef icfg_call_edge_callsite(value edg_int) {
  icfg_edge_t edg = (icfg_edge_t) Nativeint_val(edg_int);
  return llvm::wrap(llvm::cast<CallCFGEdge>(edg->edge)->getCallSite().getInstruction());
}