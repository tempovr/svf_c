#ifndef BB697042_FCEC_48AC_BE9F_D729A1130500
#include "SVF-FE/LLVMUtil.h"
#include "WPA/Andersen.h"

struct svfmodule {
  SVFModule *mod;
};

struct flowres {
  FlowSensitive *res;
};

struct pag {
  PAG *grph;
};

struct pag_node {
  const PAGNode *nd;
};

struct pag_node_iter {
  PAG::const_iterator end;
  PAG::const_iterator iter;
};

struct pag_edge_iter {
  PAGEdge::PAGEdgeSetTy::const_iterator end;
  PAGEdge::PAGEdgeSetTy::const_iterator iter;
};

struct pag_edge {
  const PAGEdge *edg;
};

struct icfg {
  ICFG *grph;
};

struct icfg_iter {
  GenericICFGTy::const_iterator end;
  GenericICFGTy::const_iterator iter;
};

struct icfg_node {
  ICFGNode *nd;
};

struct icfg_pag_nd_iter {
  std::vector<const PAGNode *>::const_iterator end;

  std::vector<const PAGNode *>::const_iterator iter;
};

struct icfg_pag_edge_iter {
  std::vector<const PAGEdge *>::const_iterator end;

  std::vector<const PAGEdge *>::const_iterator iter;
};

struct nodeid_iter {
  NodeBS::iterator end;
  NodeBS::iterator iter;
};

struct icfg_edge_iter {
 ICFGEdge::ICFGEdgeSetTy::iterator  end;
  ICFGEdge::ICFGEdgeSetTy::iterator  iter;
};

struct icfg_edge {
  const ICFGEdge *edge;
};

#define BB697042_FCEC_48AC_BE9F_D729A1130500
#endif /* BB697042_FCEC_48AC_BE9F_D729A1130500 */
