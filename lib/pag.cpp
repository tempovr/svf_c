#include "SVF-FE/LLVMUtil.h"
#include "WPA/Andersen.h"
#include "wrappers.h"
#include <llvm-c/Core.h>
#include <pag.h>

pag_t pag_get(flowres_t fpta) { return new pag{fpta->res->getPAG()}; }

pag_node_iter_t pag_iter(pag_t gh) {
  return new pag_node_iter{gh->grph->end(), gh->grph->begin()};
}

void pag_iter_next(pag_node_iter_t iter) { iter->iter++; }

void pag_iter_destroy(pag_node_iter_t iter) { delete iter; }

bool pag_iter_end(pag_node_iter_t iter) { return iter->iter == iter->end; }

unsigned pag_node_id(pag_node_t nd) { return nd->nd->getId(); }

pag_node_t pag_iter_curr(pag_node_iter_t iter) {
  return new pag_node{iter->iter->getSecond()};
}

void pag_node_destroy(pag_node_t nd) { delete nd; }

PNODEK pag_node_type(pag_node_t nd) {
  switch (nd->nd->getNodeKind()) {
  case PAGNode::ValNode:
    return ValNode;
  case PAGNode::RetNode:
    return RetNode;
  case PAGNode::ObjNode:
    return ObjNode;
  case PAGNode::VarargNode:
    return VarargNode;
  case PAGNode::GepValNode:
    return GepValNode;
  case PAGNode::GepObjNode:
    return GepObjNode;
  case PAGNode::FIObjNode:
    return FIObjNode;
  case PAGNode::DummyObjNode:
    return DummyObjNode;
  case PAGNode::DummyValNode:
    return DummyValNode;
  default:
    assert(false);
  }
}

bool pag_has_value(pag_node_t nd) { return nd->nd->hasValue(); }

pag_edge_iter_t pag_out_edges(pag_node_t nd) {
  return new pag_edge_iter{nd->nd->OutEdgeEnd(), nd->nd->OutEdgeBegin()};
}

pag_edge_t pag_edge_curr(pag_edge_iter_t iter) {
  return new pag_edge{*iter->iter};
}

void pag_edge_iter_destroy(pag_edge_iter_t iter) { delete iter; }

void pag_edge_next(pag_edge_iter_t iter) { iter->iter++; }

void pag_edge_destroy(pag_edge_t edg) { delete edg->edg; }

pag_node_t pag_edge_src(pag_edge_t edg) {
  return new pag_node{edg->edg->getSrcNode()};
}

pag_node_t pag_edge_dst(pag_edge_t edg) {
  return new pag_node{edg->edg->getDstNode()};
}

PEDGEK pag_edge_type(pag_edge_t edg) {
  switch (edg->edg->getEdgeKind()) {
  case PAGEdge::Addr:
    return Addr;
  case PAGEdge::Copy:
    return Copy;
  case PAGEdge::Store:
    return Store;
  case PAGEdge::Load:
    return Load;
  case PAGEdge::Ret:
    return Ret;
  case PAGEdge::Call:
    return Call;
  case PAGEdge::NormalGep:
    return NormalGep;
  case PAGEdge::VariantGep:
    return VariantGep;
  case PAGEdge::ThreadFork:
    return ThreadFork;
  case PAGEdge::ThreadJoin:
    return ThreadJoin;
  case PAGEdge::Cmp:
    return Cmp;
  case PAGEdge::BinaryOp:
    return BinaryOp;
  default:
    printf("%d\n", edg->edg->getEdgeKind());
    assert(false);
  }
}

bool pag_edge_iter_end(pag_edge_iter_t iter) { return iter->iter == iter->end; }

/*
LLVMValueRef pag_get_value(pag_node_t nd) {
  return llvm::wrap(nd->nd->getValue());
}*/

CAMLprim LLVMValueRef pag_get_value_caml(value nd_int) {
  pag_node_t nd = (pag_node_t) Nativeint_val(nd_int);
  assert(nd->nd->hasValue());
  return llvm::wrap(nd->nd->getValue());
}
