#include "SVF-FE/LLVMUtil.h"
#include "WPA/Andersen.h"
#include <algorithm>
#include <svfbuild.h>

extern "C" {

#include "wrappers.h"

svfmodule_t build_svf_module(const char **const mods, size_t n) {
  std::vector<std::string> modnameVec;
  std::transform(mods, mods + n, std::back_inserter(modnameVec),
                 [](const char *nstr) { return std::string(nstr); });

  SVFModule *mod =
      LLVMModuleSet::getLLVMModuleSet()->buildSVFModule(modnameVec);
  return new svfmodule{mod};
}

void destroy_svf_module(svfmodule_t mod) {
  delete mod->mod;
  delete mod;
}

flowres_t create_flowsensitive_results(svfmodule_t mod) {
  FlowSensitive *res = new FlowSensitive();
  res->analyze(mod->mod);
  return new flowres{res};
}

void destroy_flowsensitive_results(flowres_t fpta) {
  delete fpta->res;
  delete fpta;
}

nodeid_iter_t get_points_to_iter(flowres_t fpta, NodeID nid) {
  return new nodeid_iter{fpta->res->getPts(nid).end(),
                         fpta->res->getPts(nid).begin()};
}

uint32_t nodeid_iter_curr(nodeid_iter_t iter) { return *iter->iter; }
void nodeid_iter_next(nodeid_iter_t iter) { iter->iter++; }
void nodeid_iter_destroy(nodeid_iter_t iter) { delete iter; }
bool nodeid_iter_end(nodeid_iter_t iter) { return iter->end == iter->iter; }
}