#include "munit.h"
#include <llvm-c/Types.h>
#include <pag.h>
#include <stdio.h>
#include <svfbuild.h>

int main(void) { /* Use µnit here. */
  char *loop_phi = alloca(30);
  loop_phi = "../../examples/simple_loop.ll";
  char *modnames[1] = {loop_phi};
  svfmodule_t mod = build_svf_module(modnames, 1);
  flowres_t fpta = create_flowsensitive_results(mod);
  pag_t pag_graph = pag_get(fpta);
  pag_node_iter_t nd_iter = pag_iter(pag_graph);

  while (!pag_iter_end(nd_iter)) {
    pag_node_t nd = pag_iter_curr(nd_iter);

    if (pag_has_value(nd)) {
      //LLVMValueRef t = pag_get_value_caml(nd);
      //LLVMDumpValue(t);
    }

    pag_node_destroy(nd);
    pag_iter_next(nd_iter);
  }
  pag_iter_destroy(nd_iter);
  destroy_svf_module(mod);
  return 0;
  ;
}