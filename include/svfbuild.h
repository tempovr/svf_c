#ifndef D606290D_DC3A_4D98_87A8_94F38A936A08
#define D606290D_DC3A_4D98_87A8_94F38A936A08
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

struct svfmodule;
typedef struct svfmodule *svfmodule_t;

svfmodule_t build_svf_module(const char **mods, size_t n);
void destroy_svf_module(svfmodule_t mod);

struct flowres;
typedef struct flowres *flowres_t;
flowres_t create_flowsensitive_results(svfmodule_t mod);
void destroy_flowsensitive_results(flowres_t fpta);

struct nodeid_iter;
typedef struct nodeid_iter *nodeid_iter_t;

nodeid_iter_t get_points_to_iter(flowres_t fpta, uint32_t nid);

uint32_t nodeid_iter_curr(nodeid_iter_t iter);
bool nodeid_iter_end(nodeid_iter_t iter);
void nodeid_iter_next(nodeid_iter_t iter);
void nodeid_iter_destroy(nodeid_iter_t iter);

#ifdef __cplusplus
}
#endif
#endif /* D606290D_DC3A_4D98_87A8_94F38A936A08 */
