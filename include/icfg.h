#ifndef FE8DB9FE_DF06_4EA8_BBB9_69E3C60948D7
#define FE8DB9FE_DF06_4EA8_BBB9_69E3C60948D7
#include <llvm-c/Types.h>
#define CAML_NAME_SPACE
#include "caml/fail.h"
#include "caml/mlvalues.h"
#include "pag.h"

#ifdef __cplusplus
extern "C" {
#endif
struct icfg;
typedef struct icfg *icfg_t;

icfg_t get_icfg(pag_t grph);

struct icfg_iter;
typedef struct icfg_iter *icfg_iter_t;

icfg_iter_t get_icfg_iter(icfg_t cfg);

void icfg_next(icfg_iter_t iter);

struct icfg_node;
typedef icfg_node *icfg_node_t;

icfg_node_t icfg_curr(icfg_iter_t iter);


void icfg_intra_addrprint(icfg_node_t nd);

unsigned icfg_node_id(icfg_node_t nd);

typedef enum ICFGNodeType {
  IntraBlock = 0,
  FunEntryBlock = 1,
  FunExitBlock = 2,
  FunCallBlock = 3,
  FunRetBlock = 4
} ICFGNodeType;

ICFGNodeType get_icfg_node_ty(icfg_node_t nd);

bool icfg_iter_end(icfg_iter_t iter);

CAMLprim LLVMValueRef icfg_node_get_function(value nd);

CAMLprim LLVMValueRef icfg_node_get_callee(value nd);

CAMLprim LLVMValueRef icfg_intra_insn(value nd);

struct icfg_pag_nd_iter;
typedef icfg_pag_nd_iter *icfg_pag_nd_iter_t;

struct icfg_pag_edge_iter;
typedef icfg_pag_edge_iter *icfg_pag_edge_iter_t;

icfg_pag_edge_iter_t icfg_intra_edges(icfg_node_t nd);

icfg_pag_nd_iter_t icfg_callblock_params(icfg_node_t nd);

CAMLprim LLVMValueRef icfg_call_getcallsite(value nd);

icfg_pag_nd_iter_t icfg_funentry_params(icfg_node_t nd);

pag_node_t icfg_ret_actual(icfg_node_t nd);

CAMLprim LLVMValueRef icfg_ret_getcallsite(value nd);

pag_node_t icfg_funexit_ret(icfg_node_t nd);

void icfg_iter_destroy(icfg_iter_t iter);

void icfg_pag_nd_iter_next(icfg_pag_nd_iter_t iter);
void icfg_pag_nd_iter_destroy(icfg_pag_nd_iter_t iter);
pag_node_t icfg_pag_nd_iter_curr(icfg_pag_nd_iter_t iter);

bool icfg_pag_nd_iter_end(icfg_pag_nd_iter_t end);

pag_edge_t icfg_pag_edge_iter_curr(icfg_pag_edge_iter_t iter);

void icfg_pag_edge_iter_destroy(icfg_pag_edge_iter_t iter);

void icfg_pag_edge_iter_next(icfg_pag_edge_iter_t iter);

bool icfg_pag_edge_iter_end(icfg_pag_edge_iter_t iter);

struct icfg_edge_iter;
typedef icfg_edge_iter *icfg_edge_iter_t;

struct icfg_edge;
typedef icfg_edge *icfg_edge_t;

icfg_edge_iter_t get_out_edge_iter(icfg_node_t nd);
bool icfg_edge_iter_end(icfg_edge_iter_t iter);
icfg_edge_t icfg_edge_curr(icfg_edge_iter_t iter);
void icfg_edge_next(icfg_edge_iter_t iter);
void icfg_edge_iter_destroy(icfg_edge_iter_t iter);



typedef enum ICFGEdgeTy {
  IntraCF = 0,
  CallCF = 1,
  RetCF = 2,
} ICFGEdgeTy;

ICFGEdgeTy icfg_edge_getkind(icfg_edge_t);

icfg_node_t icfg_edge_src(icfg_edge_t);
icfg_node_t icfg_edge_dst(icfg_edge_t);

CAMLprim LLVMValueRef icfg_ret_edge_callsite(value edg);
CAMLprim LLVMValueRef icfg_call_edge_callsite(value edg);

//void icfg_intra_addrprint(icfg_node_t nd);
#ifdef __cplusplus
}
#endif
#endif /* FE8DB9FE_DF06_4EA8_BBB9_69E3C60948D7 */
