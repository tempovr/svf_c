#ifndef BB2E032D_096D_410A_83CF_F5E4B83A5C0B
#define BB2E032D_096D_410A_83CF_F5E4B83A5C0B
#include "svfbuild.h"
#include <llvm-c/Types.h>
#define CAML_NAME_SPACE
#include "caml/fail.h"
#include "caml/mlvalues.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum PNODEK {
  ValNode = 0,
  ObjNode = 1,
  RetNode = 2,
  VarargNode = 3,
  GepValNode = 4,
  GepObjNode = 5,
  FIObjNode = 6,
  DummyValNode = 7,
  DummyObjNode = 8, // due to ffi these values cannot change so better to set
                    // them equal in case of more values etc.
} PNODEK;

typedef enum PEDGEK {
  Addr = 0,
  Copy = 1,
  Store = 2,
  Load = 3,
  Call = 4,
  Ret = 5,
  NormalGep = 6,
  VariantGep = 7,
  ThreadFork = 8,
  ThreadJoin = 9,
  Cmp = 10,
  BinaryOp = 11
} PEDGEK;

struct pag;

typedef struct pag *pag_t;

pag_t pag_get(flowres_t fpta);

struct pag_node_iter;
typedef struct pag_node_iter *pag_node_iter_t;
pag_node_iter_t pag_iter(pag_t gh);

struct pag_node;
typedef struct pag_node *pag_node_t;
pag_node_t pag_iter_curr(pag_node_iter_t iter);

bool pag_iter_end(pag_node_iter_t iter);

// takes ownership
void pag_iter_next(pag_node_iter_t iter);

void pag_iter_destroy(pag_node_iter_t iter);

unsigned pag_node_id(pag_node_t nd);

PNODEK pag_node_type(pag_node_t nd);

void pag_node_destroy(pag_node_t nd);

//LLVMValueRef pag_get_value(pag_node_t nd);
CAMLprim LLVMValueRef pag_get_value_caml(value nd);

bool pag_has_value(pag_node_t nd);

struct pag_edge_iter;

typedef struct pag_edge_iter *pag_edge_iter_t;

pag_edge_iter_t pag_out_edges(pag_node_t nd);

void pag_edge_next(pag_edge_iter_t iter);

void pag_edge_iter_destroy(pag_edge_iter_t iter);

struct pag_edge;
typedef struct pag_edge *pag_edge_t;

pag_edge_t pag_edge_curr(pag_edge_iter_t iter);

void pag_edge_destroy(pag_edge_t edg);

pag_node_t pag_edge_src(pag_edge_t edg);
pag_node_t pag_edge_dst(pag_edge_t edg);
PEDGEK pag_edge_type(pag_edge_t edg);

bool pag_edge_iter_end(pag_edge_iter_t iter);

#ifdef __cplusplus
}
#endif
#endif /* BB2E032D_096D_410A_83CF_F5E4B83A5C0B */
